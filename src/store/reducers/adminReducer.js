const defaultState = {
    users: [],
    posts: [],
    post: {},
}

const admin = (state = defaultState, action) => {
    switch (action.type) {
        case 'GOT_USERS':
            return {
                ...state,
                users: action.payload
            }
        case 'GOT_POSTS':
            return {
                ...state,
                posts: action.payload
            }
        case 'POST_ADDED':
            return {
                ...state,
                posts: state.posts.concat(action.payload),
                post: action.payload
            }
        case 'GOT_SINGLE_POST':
            return {
                ...state,
                post: action.payload
            }
        case 'UPDATED_POST':
            return {
                ...state,
                posts: state.posts.map(p => {
                    if (p.id === action.payload.id) {
                        //returns the posts array without p 
                        //and currently post in payload
                        return {
                            ...p,
                            ...action.payload
                        }
                    } else {
                        return p
                    }
                }),
                post: action.payload
            }
        case 'UPLOADED_IMAGE':
            return {
                ...state,
                post: {
                    ...state.post,
                    postImage: [action.payload]
                }
            }
        default: return state
    }
}

export default admin;