import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import PageWrapper from './Components/PageWrapper';
import AdminWrapper from './Components/AdminWrapper';
import LoginWrapper from './Components/LoginWrapper';

//Pages
import Home from './Components/Pages/Home';
import Login from './Components/Pages/Login';

//AdminPages
import Dashboard from './Components/Pages/Admin/Dashboard';
import Users from './Components/Pages/Admin/Users';
import Posts from './Components/Pages/Admin/Posts';
import AddPost from './Components/Pages/Admin/AddPost';

class App extends Component {
  render() {
    return (
      <Router>
        <Route
          exact path='/admin/users'
          render={props => {
            return (
              <div>
                {this.props.auth.token ?
                  <AdminWrapper>
                    <Users />
                  </AdminWrapper >
                  :
                  <LoginWrapper>
                    <Login />
                  </LoginWrapper>
                }
              </div>
            )
          }}
        />

        <Route
          exact path='/admin/posts/:view/:id'
          render={props => {
            return (
              <div>
                {this.props.auth.token ?
                  <AdminWrapper>
                    <AddPost />
                  </AdminWrapper >
                  :
                  <LoginWrapper>
                    <Login />
                  </LoginWrapper>
                }
              </div>
            )
          }}
        />

        <Route
          exact path='/admin/posts/:view'
          render={props => {
            return (
              <div>
                {this.props.auth.token ?
                  <AdminWrapper>
                    <AddPost />
                  </AdminWrapper >
                  :
                  <LoginWrapper>
                    <Login />
                  </LoginWrapper>
                }
              </div>
            )
          }}
        />

        <Route
          exact path='/admin/posts'
          render={props => {
            return (
              <div>
                {this.props.auth.token ?
                  <AdminWrapper>
                    <Posts />
                  </AdminWrapper >
                  :
                  <LoginWrapper>
                    <Login />
                  </LoginWrapper>
                }
              </div>
            )
          }}
        />

        <Route
          exact path='/admin'
          render={props => {
            return (
              <div>
                {this.props.auth.token ?
                  <AdminWrapper>
                    <Dashboard />
                  </AdminWrapper >
                  :
                  <LoginWrapper>
                    <Login />
                  </LoginWrapper>
                }
              </div>
            )
          }}
        />

        <Route
          exact path="/"
          render={props => (
            <PageWrapper>
              <Home />
            </PageWrapper>
          )}
        />
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
