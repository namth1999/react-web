import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import * as AdminActions from '../../../store/actions/adminActions';
import Paper from '@material-ui/core/Paper';
import { withFormik, Form } from 'formik';
import { withRouter } from 'react-router-dom';
import * as Yup from 'yup';
import { FormikTextField, FormikSelectField } from 'formik-material-fields';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import ImageIcon from '@material-ui/icons/Image';
import API from '../../../utils/api';

/* global $ */
const styles = theme => ({
    container: {
        margin: theme.spacing.unit * 3,
        display: 'flex',
        flexDirection: 'row wrap',
        width: '100%'
    },
    save: {
        marginBottom: theme.spacing.unit * 2
    },
    formControl: {
        margin: theme.spacing.unit
    }, leftSide: {
        flex: 2,
        height: '100%',
        margin: theme.spacing.unit * 1,
        padding: theme.spacing.unit * 3
    }, rightSide: {
        flex: 1,
        height: '100%',
        margin: theme.spacing.unit * 1,
        padding: theme.spacing.unit * 3
    }
})

class AddPost extends Component {
    componentDidUpdate(props, state) {
        if (this.props.match.params.view === 'add'
            && this.props.admin.posts.filter(
                p => p.title === this.props.values.title).length > 0) {
            const post = this.props.admin.posts.filter(p => p.title === this.props.values.title[0]);
            this.props.history.push('/admin/posts/edit' + post.id);
        }

        if (this.props.admin.post.id !== props.admin.post.id) {
            this.props.setValues(this.props.admin.post);
        }
    }

    componentDidMount(props, state) {
        if (this.props.match.params.view === 'edit'
            && this.props.match.params.id) {
            this.props.getSinglePost(this.props.match.params.id, this.props.auth.token);
        }
    }

    uploadImange = (e) => {
        const data = new FormData();
        data.append('file', e.target.files[0], new Date().getTime().toString() + e.target.files[0].name);
        this.props.uploadImage(data, this.props.auth.token, this.props.admin.post.id, this.props.auth.user.userId);
    }

    render() {
        const { classes } = this.props;
        return (
            <Form className={classes.container}>
                <Paper className={classes.leftSide}>
                    <FormikTextField
                        name="title"
                        label="Title"
                        margin="normal"
                        onChange={e => this.props.setFieldValue('slug', e.target.value.toLowerCase().replace(/ /g, `_`))}
                        fullWidth
                    />
                    <FormikTextField
                        name="slug"
                        label="Slug"
                        margin="normal"
                        fullWidth
                    />
                    <FormikTextField
                        name="content"
                        label="Content"
                        margin="normal"
                        fullWidth
                    />
                </Paper>
                <Paper className={classes.rightSide}>
                    <FormikSelectField
                        name="status"
                        label="Status"
                        margin="normal"
                        fullWidth
                        options={[
                            { label: 'Unpublished', value: false },
                            { label: 'Published', value: true }
                        ]}
                    />
                    <div className={classes.save}>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={e => {
                                this.props.handleSubmit()
                            }}
                        >
                            <SaveIcon />
                            Save
                        </Button>
                    </div>
                    {/* {this.props.admin.post.postImage ?
                        <img src={API.makeFileUrl(this.props.admin.post.postImage[0].url, this.props.auth.token)}
                            className="post-image"
                            alt=""
                        />
                        : null} */}
                    <div>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={e => {
                                $('.myFile').trigger('click');
                            }}
                        >
                            <ImageIcon />
                            Upload Post Image
                        </Button>
                        <input
                            type="file"
                            style={{ display: 'none' }}
                            className="myFile"
                            onChange={this.uploadImange}
                        />
                    </div>
                </Paper>
            </Form>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        admin: state.admin
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addPost: (post, token) => {
            dispatch(AdminActions.addPost(post, token));
        },
        getSinglePost: (id, token) => {
            dispatch(AdminActions.getSinglePost(id, token));
        },
        updatePost: (post, token) => {
            dispatch(AdminActions.updatePost(post, token));
        },
        uploadImage: (data, token, postId, userId) => {
            dispatch(AdminActions.uploadImage(data, token, postId, userId));
        }
    }
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(withFormik({
    mapPropsToValues: (props) => ({
        title: props.admin.post.title || '',
        slug: props.admin.post.slug || '',
        createdAt: props.admin.post.createdAt || '',
        status: props.admin.post.status || false,
        content: props.admin.post.content || ''
    }),
    validationSchema: Yup.object().shape({
        title: Yup.string().required('Title is required'),
        slug: Yup.string().required('Slug is required'),
        content: Yup.string().required('Content is required'),
    }),
    handleSubmit: (values, { setSubmitting, props }) => {
        if (props.match.params.view === 'edit') {
            const post = {
                ...values,
                id: props.match.params.id
            }
            props.updatePost(post, props.auth.token)
        } else {
            props.addPost(values, props.auth.token);
        }
    }
})(withStyles(styles)(AddPost))));
