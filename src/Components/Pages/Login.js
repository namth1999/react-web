import React, { Component } from 'react';
import Field from '../Common/ContactField';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import * as AuthActions from '../../store/actions/authActions';

const fields = [
    { name: 'email', elementName: 'input', type: 'email', id: 'email', placeholder: 'Your email' },
    { name: 'password', elementName: 'input', type: 'password', id: 'password', placeholder: 'Your password' },
]

class Login extends Component {
    render() {
        return (
            <div className="login-page">
                <div className="container">
                    <div className="login-form">
                        <div className="row">
                            <h1>Login</h1>
                        </div>
                        <div className="row">
                            <form onSubmit={e => {
                                e.preventDefault();
                                this.props.login(this.props.values.email, this.props.values.password);
                            }}>
                            {fields.map((f, i) => {
                                return (
                                    <div className="col-md-12">
                                        <Field
                                            key={i}
                                            {...f}
                                            value={this.props.values[f.id]}
                                            onChange={this.props.handleChange}
                                            name={f.name}
                                            onBlur={this.props.handleBlur}
                                            touched={this.props.touched[f.id]}
                                            errors={this.props.errors[f.id]}
                                        />
                                    </div>
                                )
                            })}
                            <div className="col-md-12">
                                <button className="btn btn-primary">Login</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, pass) => { //this.props.login
      dispatch(AuthActions.logIn(email,pass));
    }
  }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
    }),
    validationSchema: Yup.object().shape({
        password: Yup.string().min(3, 'At least 3 char').required('A password must be given'),
        email: Yup.string().email('Email is not valid').required('An email must be given'),
    })
})(Login));
