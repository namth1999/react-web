import React, { Component } from 'react';
import Header from './Header';
import AboutListItem from './AboutListItem';
import image from '../assets/img/about.jpg';

import imgAbout1 from '../assets/img/about/1.jpg';
import imgAbout2 from '../assets/img/about/2.jpg';
import imgAbout3 from '../assets/img/about/3.jpg';
import imgAbout4 from '../assets/img/about/4.jpg';


const listItem = [
    {heading:'2009-2011', subheading: 'Our Humble Beginnings', description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',listItemClassName:'', image: imgAbout1},
    {heading:'March 2011', subheading: 'An Agency is Born', description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',listItemClassName:'timeline-inverted', image:imgAbout2},
    {heading:'December 2012', subheading: 'Transition to Full Service', description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',listItemClassName:'', image:imgAbout3},
    {heading:'July 2014', subheading: 'Phase Two Expansion', description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',listItemClassName:'timeline-inverted', image:imgAbout4}
];

class About extends Component {
    render() {
        return (
            <div>
                <Header
                    title="About us"
                    subtitle="It's really a great story"
                    showButton={false}
                    image={image}
                />
                <section className="page-section" id="about">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h2 className="section-heading text-uppercase">About</h2>
                                <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <ul className="timeline">
                                    {listItem.map((item,i)=>{
                                        return <AboutListItem {...item} key={i}/>
                                    })}
                                    <li className="timeline-inverted">
                                        <div className="timeline-image">
                                            <h4>Be Part
                                        <br />Of Our
                                        <br />Story!</h4>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default About;