import React, { Component } from 'react';
import ContactField from './ContactField';
import { withFormik } from 'formik';
import * as Yup from 'yup';

const contactFields = {
    sections: [
        [
            { name: 'name', elementName: 'input', type: 'text', id: 'name', placeholder: 'Your name *' },
            { name: 'email', elementName: 'input', type: 'email', id: 'email', placeholder: 'Your email *' },
            { name: 'phone', elementName: 'input', type: 'text', id: 'phone', placeholder: 'Your phone number *' },
        ],
        [
            { name: 'message', elementName: 'textarea', type: 'text', id: 'message', placeholder: 'Type your message *' }
        ]

    ]
}
class Contact extends Component {
    render() {
        return (
            <section className="page-section" id="contact">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <h2 className="section-heading text-uppercase">Contact Us</h2>
                            <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <form name="sentMessage" novalidate="novalidate" onSubmit={(e) => this.props.handleSubmit(e)}>
                                <div className="row">
                                    {contactFields.sections.map((section, sectionIndex) => {
                                        return (
                                            <div className="col-md-6" key={sectionIndex}>
                                                {section.map((field, i) => {
                                                    return <ContactField
                                                        {...field}
                                                        key={i}
                                                        value={this.props.values[field.id]}
                                                        onChange={this.props.handleChange}
                                                        onBlur={this.props.handleBlur}
                                                        touched={this.props.touched[field.id]}
                                                        errors={this.props.errors[field.id]}
                                                    />
                                                })}
                                            </div>

                                        )
                                    })}
                                    <div className="clearfix"></div>
                                    <div className="col-lg-12 text-center">
                                        <div id="success"></div>
                                        <button
                                            className="btn btn-primary btn-xl text-uppercase"
                                            type="submit"
                                        >Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}



export default withFormik({
    mapPropsToValues: () => ({//pass p to an object
        name: '',
        email: '',
        phone: '',
        message: '',
    }),
    validationSchema: Yup.object().shape({
        name: Yup.string().min(3, 'At least 3 char').required('A name must be given.'),
        email: Yup.string().email('Email is not valid').required('An email must be given.'),
        phone: Yup.string().min(9, 'Please provide 9 digit number').required('A phone number must be given.'),
        message: Yup.string().required('A message must be given')
    }),
    handleSubmit: (values, { setSubmitting }) => {
        alert("You have submitted the form");
    }
})(Contact);